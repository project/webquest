<fieldset id="conclusion-description-<?php print $node->nid?>" class="collapsible">
  <legend>Conclusion</legend>
  <span><?php print check_markup($node->conclusion_description, FILTER_FORMAT_DEFAULT, FALSE) ?></span>
</fieldset>