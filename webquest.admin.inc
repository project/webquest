<?php

/**
 * @file
 * Admin page callbacks for the webquest module.
 */

/**
 * Returns webquest admin page .
 */
function webquest_admin() {
  $form['webquest_default_environment'] = array(
    '#type' => 'select', 
    '#title' => t('Default environment'), 
    '#required' => TRUE,
    '#options' => array("Sugar","Desktop"),
    '#default_value' =>  variable_get('webquest_default_environment', 'Sugar'), 
  );
  
  return system_settings_form($form);
}

