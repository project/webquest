<?php  if (sizeof($node->process) > 0): ?>
<fieldset id="process-description-<?php print $node->nid?>" class="collapsible">
  <legend>Roles</legend>
  <ul>
  <?php
      $env = _webquest_get_environment($node->environment);
      foreach ($node->process as $process):
  ?>
    <li><?php print $process['role']?>: <?php print $process['text']?> (<?php print $env[$process['resource']]?>)</li>
  <?

      endforeach; 
  ?>
  </ul>
</fieldset>
<?php endif;?>
