<fieldset id="intro-description-<?php print $node->nid?>" class="collapsible">
  <legend>Introduction</legend>
  <span><?php print check_markup($node->intro_description, FILTER_FORMAT_DEFAULT, FALSE) ?></span>
</fieldset>