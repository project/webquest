<?php if (sizeof($node->task) > 0): ?>
  <ul>
  <?php
      $res = _webquest_get_environment($node->environment);
      foreach ($node->task as $task):
  ?>
    <li><?php print $task['text']?> (<?php print $res[$task['resource']]?>)</li>
  <?
      endforeach;
  ?>
  </ul>
<?php endif; ?>
