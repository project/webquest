<fieldset id="evaluation-description-<?php print $node->nid?>" class="collapsible">
  <legend>Evaluation</legend>
  <span><?php print check_markup($node->evaluation_description, FILTER_FORMAT_DEFAULT, FALSE) ?></span>
</fieldset>