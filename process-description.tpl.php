<fieldset id="process-description-<?php print $node->nid?>" class="collapsible">
  <legend>Process</legend>
  <span><?php print check_markup($node->process_description, FILTER_FORMAT_DEFAULT, FALSE) ?></span>
  <?php print theme('webquest_process_view',$node) ?>
</fieldset>
