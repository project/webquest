<fieldset id="task-description-<?php print $node->nid?>" class="collapsible">
  <legend>Tasks</legend>
  <span><?php print check_markup($node->task_description, FILTER_FORMAT_DEFAULT, FALSE) ?></span>
  <?php print theme('webquest_tasks_view',$node) ?>
</fieldset>